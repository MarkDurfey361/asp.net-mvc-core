﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Core_Crud_MVC.Models;

namespace Core_Crud_MVC.Controllers
{
    public class EmpController : Controller
    {

        /**
         * Se crea una instancia nueva del contexto.
         */
        private readonly ApplicationDbContext _db;

        #region Constructor
        /**
         * Se inyecta la dependencia en el constructor del controlador.
         */
        public EmpController(ApplicationDbContext db)
        {
            _db = db;
        }
        #endregion

        #region Index Listado y búsqueda
        /**
         * Devuelve el listado de los empleados obtenidos de la base de datos.
        */
        public IActionResult Index()
        {
            var displayData = _db.EmployeeTable.ToList();
            return View(displayData);
        }

        /**
         * Devuelve una búsqueda dinámica por cada campo
         * Recibe un string, con él busca en los registros 
         * aquellos los que cotengan esa cadena como 
         * coincidencia y los muestra. Busca en todos los campos.
         */
        [HttpGet]
        public async Task<IActionResult> Index(string busqueda)
        {
            /*
             * Se agrega una propiedad al ViewData que apuntará al valor
             * que hayamos ingresado para buscar.
             */
            ViewData["GetEmployeeDetails"] = busqueda;

            /*
             * Con una expresión LINQ, se indica la tabla sobre la cual se quiere
             * trabajar. Usando la clase contexto como puente obtiene los datos de la misma.
             * 
             * Se almacenan los datos de la tabla en una variable de tipo implícito, es decir
             * que el compilador determinará el tipo de variable que será.
             */
            var query = from x in _db.EmployeeTable select x;

            /*
             * Se evalúa si se intenta realizar una búsqueda.
             */
            if (!String.IsNullOrEmpty(busqueda))
            {
                Console.WriteLine(query.GetType());
                /*
                 * Se filtran los registros de la tabla contenidos en la variable query
                 * Con el método LINQ where y una expresión Lambda como predicado se realiza el filtrado.
                 */
                query = query.Where(x => x.EmpName.Contains(busqueda) || x.Email.Contains(busqueda) 
                || x.Age.ToString().Contains(busqueda) || x.Salary.ToString().Contains(busqueda));
            }

            /*
             * Regresa una lista con los registros filtrados y por medio del método AsNoTracking evita
             * que el DbContext persista de cambios.
             */
            return View(await query.AsNoTracking().ToListAsync());
        } 
        #endregion

        #region Create Vista crear empleado
        /**
         * Muestra el formulario de registro de empleado.
         */
        public IActionResult Create()
        {
            return View();
        }

        /**
         * Método que registra un nuevo empleado.
         * Recibe un modelo para trabajar.
         * En este caso el modelo es de tipo NewEmpClass.
         */
        [HttpPost]
        public async Task<IActionResult> Create(NewEmpClass emp)
        {
            /*
             * Valida que el modelo recibido no contenga errores.
             */
            if (ModelState.IsValid)
            {
                /*
                 * Con el método Add del context se prepara el registro a agregar.
                 */
                _db.Add(emp);
                /*
                 * Método SaveChangesAsync de DbContext guarda los cambios realizados
                 * tras la inserción del registro.
                 */
                await _db.SaveChangesAsync();
                /*
                 * Se envía de vuelta al Lstado para corroborar el nuevo registro.
                 */
                return RedirectToAction("Index");
            }
            return View(emp);
        }
        #endregion

        #region Edit editar empleado
        /**
         * Obtiene la información del empleado cuyo Id recibe por verbo Get.
         * Renderiza los datos obtenidos en un formulario para editar.
         */
        public async Task<IActionResult> Edit(int? id)
        {
            /*
             * Verifica que se esté intentando editar un registro.
             */
            if(id == null)
            {
                /*
                 * Si falla al recuperar el Id retorna la lista de empleados.
                 */
                return RedirectToAction("Index");
            }

            /*
             * En una variable de tipo implícito busca y almacena los datos del empleado
             * referenciado al Id recibido.
             */
            var getEmployeeDetails = await _db.EmployeeTable.FindAsync(id);

            /*
             * Devuelve el formulario pre-edición con los datos renderizados.
             */
            return View(getEmployeeDetails);
        }

        /**
         * Modifica un registro de la base de datos.
         * Recibe un modelo ipo NewEmpClass, que contiene la información del empleado actualizada.
         */
        [HttpPost]
        public async Task<IActionResult> Edit(NewEmpClass emp)
        {
            /*
             * Verifica que la información recibida no contiene fallos.
             */
            if (ModelState.IsValid)
            {
                /*
                 * Ejecuta el método Update pasándo como parámetro, el modelo actualizado.
                 */
                _db.Update(emp);
                /*
                 * Se pide guardar cambios.
                 */
                await _db.SaveChangesAsync();

                /*
                 * Se redirige a la lista para verificar la actualización de datos.
                 */
                return RedirectToAction("Index");
            }
            
            /*
             * Si existen errores de validación, se retorna la misma vista y se muestran
             * los mensajes de error creados.
             */
            return View(emp);
        }
        #endregion

        #region Details detalles del empleado
        /**
         * Muestra la información de un registro de la base de datos.
         * Recibe un Id del empleado a buscar.
         */
        public async Task<IActionResult> Details(int? id)
        {
            /*
             * Se almacenan los dato y se bsuca el registro indicado.
             */
            if (id == null)
            {

                return RedirectToAction("Index");
            }

            /*
             * Se almacenan los dato y se bsuca el registro indicado.
             */
            var getEmployeeDetails = await _db.EmployeeTable.FindAsync(id);
            return View(getEmployeeDetails);
        }
        #endregion

        #region Delete eliminar un empleado
        /**
         * Muestra la información de un registro de la base de datos.
         * Recibe un Id del empleado a buscar.
         * Posteriormente, se modifica la información.
         */
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            /*
             * Encuentra al empleado y lo guarda en getEmployeeDetails.
             */
            var getEmployeeDetails = await _db.EmployeeTable.FindAsync(id);
            
            /*
             * Devuelve los detalles de empleado encontrado en la vista para
             * confirmar la eliminación del mismo.
             */
            return View(getEmployeeDetails);
        }

        /**
         * Elimina la información del empleado cuyo Id es recibido.
         */
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            /*
             * Primero usa la clase contexto para ubicar al empleado.
             */
            var getEmployeeDetails = await _db.EmployeeTable.FindAsync(id);

            /*
             * El método Remove, elimina al empleado fue encontrado por el Id.
             */
            _db.EmployeeTable.Remove(getEmployeeDetails);

            /*
             * Se guardan los cambios que fueron realizados antes.
             */
            await _db.SaveChangesAsync();

            /*
             * Retorna a la lista para verificar los cambios realizados.
             */
            return RedirectToAction("Index");
        }
        #endregion

    }
}
