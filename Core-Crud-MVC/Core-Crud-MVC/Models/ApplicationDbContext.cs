﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Core_Crud_MVC.Models
{
    /**
     * Clase contexto, una clase contexto es el puente que conecta la aplicación
     * con la base de datos, relacionando las clases de dominio y las entidades
     * de la base de datos.
     */
    public class ApplicationDbContext : DbContext
    {

        /**
         * base() hace referencia a la cadena de conexión que definimos en "appsettings.json"
         */
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base (options)
        {

        }

        /**
         * Database Set, en este caso, incluye la tabla EmployeeTable de la base de datos EMP
         * El nombre de la propiedad debe ser idéntico al de la tabla de la base de datos
         */
        public DbSet<NewEmpClass> EmployeeTable { get; set; }

    }
}
