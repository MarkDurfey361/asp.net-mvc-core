﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System.Globalization;

namespace Core_Crud_MVC.Models
{
    public class NewEmpClass
    {
        [Key]
        [Display(Name = "Id empleado.")]
        public int EmpId { get; set; }

        [Required(ErrorMessage = "Ingrese nombre.")]
        [Display(Name = "Nombre empleado.")]
        public string EmpName { get; set; }

        [Required(ErrorMessage = "Ingrese Email.")]
        [Display(Name = "Email empleado.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Ingrese edad entre 20 y 50 años.")]
        [Display(Name = "Edad empleado.")]
        [Range(20, 50)]
        public int Age { get; set; }

        [Required(ErrorMessage = "Ingrese salario.")]
        [Display(Name = "Salario empleado.")]
        public int Salary { get; set; }
    }
}
